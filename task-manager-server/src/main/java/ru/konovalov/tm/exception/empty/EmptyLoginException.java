package ru.konovalov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    @NotNull
    public EmptyLoginException() {
        super("Error. Login is empty");
    }

}
