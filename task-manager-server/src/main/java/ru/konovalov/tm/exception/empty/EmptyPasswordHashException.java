package ru.konovalov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.exception.AbstractException;

public class EmptyPasswordHashException extends AbstractException {

    @NotNull
    public EmptyPasswordHashException() {
        super("Error. Password hash is empty");
    }

}
