package ru.konovalov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.exception.AbstractException;

public class EmptyIndexException extends AbstractException {

    @NotNull
    public EmptyIndexException() {
        super("Error. Index is empty");
    }

}
