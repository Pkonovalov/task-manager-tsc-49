package ru.konovalov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.exception.AbstractException;

public class DatabaseInitException extends AbstractException {

    @NotNull
    public DatabaseInitException() {
        super("Error. Database initialization failed.");
    }

}
