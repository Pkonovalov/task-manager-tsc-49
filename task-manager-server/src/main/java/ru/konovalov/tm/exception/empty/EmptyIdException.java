package ru.konovalov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    @NotNull
    public EmptyIdException() {
        super("Error. Id is empty");
    }

    @NotNull
    public EmptyIdException(String value) {
        super("Error. " + value + " id is empty");
    }

}
