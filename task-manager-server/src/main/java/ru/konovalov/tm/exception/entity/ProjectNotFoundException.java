package ru.konovalov.tm.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    @NotNull
    public ProjectNotFoundException() {
        super("Error. Project not found.");
    }

}
