package ru.konovalov.tm.enumerated;

public enum EntityOperationType {
    LOAD,
    START_PERSIST,
    FINISH_PERSIST,
    START_UPDATE,
    FINISH_UPDATE,
    START_REMOVE,
    FINISH_REMOVE,
}
