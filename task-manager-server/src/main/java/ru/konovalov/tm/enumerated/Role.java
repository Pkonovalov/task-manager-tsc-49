package ru.konovalov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Role {
    USER("User"),
    ADMIN("Admin");

    @NotNull
    private final String displayName;

    @NotNull Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
