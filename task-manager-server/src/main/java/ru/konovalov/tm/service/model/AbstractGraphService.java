package ru.konovalov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.api.service.IConnectionService;
import ru.konovalov.tm.model.AbstractGraph;

public abstract class AbstractGraphService<E extends AbstractGraph> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    public AbstractGraphService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
