package ru.konovalov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.repository.model.ITaskRepository;
import ru.konovalov.tm.api.service.IConnectionService;
import ru.konovalov.tm.api.service.model.ITaskService;
import ru.konovalov.tm.enumerated.Status;
import ru.konovalov.tm.exception.empty.EmptyIdException;
import ru.konovalov.tm.exception.empty.EmptyIndexException;
import ru.konovalov.tm.exception.empty.EmptyNameException;
import ru.konovalov.tm.exception.entity.TaskNotFoundException;
import ru.konovalov.tm.model.TaskGraph;
import ru.konovalov.tm.model.UserGraph;
import ru.konovalov.tm.repository.model.TaskGraphRepository;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public final class TaskGraphService extends AbstractGraphService<TaskGraph> implements ITaskService {

    @NotNull
    public TaskGraphService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    public ITaskRepository getRepository() {
        return new TaskGraphRepository(connectionService.getEntityManager());
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskGraph> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository repository = new TaskGraphRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<TaskGraph> collection) {
        if (collection == null) return;
        for (TaskGraph item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskGraph add(@Nullable final TaskGraph entity) {
        if (entity == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository repository = new TaskGraphRepository(entityManager);
            repository.add(entity);
            entityManager.getTransaction().commit();
            return entity;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    private void update(@NotNull final TaskGraph entity, @NotNull EntityManager entityManager) {
        @NotNull final ITaskRepository repository = new TaskGraphRepository(entityManager);
        repository.update(entity);
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskGraph findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository repository = new TaskGraphRepository(entityManager);
            return repository.findById(optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository repository = new TaskGraphRepository(entityManager);
            @NotNull final List<TaskGraph> tasks = repository.findAll();
            for (TaskGraph t :
                    tasks) {
                repository.remove(t);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository repository = new TaskGraphRepository(entityManager);
            @Nullable final TaskGraph task = repository.getReference(optionalId.orElseThrow(EmptyIdException::new));
            if (task == null) return;
            repository.remove(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final TaskGraph entity) {
        if (entity == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository repository = new TaskGraphRepository(entityManager);
            @Nullable final TaskGraph task = repository.getReference(entity.getId());
            if (task == null) return;
            repository.remove(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskGraph findByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskGraphRepository(entityManager);
            return taskRepository.findByIndex(userId, index);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskGraph findByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskGraphRepository(entityManager);
            return taskRepository.findByName(userId, name);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskGraphRepository(entityManager);
            @Nullable final TaskGraph task = taskRepository.findByIndex(userId, index);
            if (task == null) return;
            taskRepository.remove(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskGraphRepository(entityManager);
            @Nullable final TaskGraph task = taskRepository.findByName(userId, name);
            if (task == null) return;
            taskRepository.remove(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph updateById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final String name, @Nullable final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskGraphRepository(entityManager);
            @NotNull final TaskGraph task = Optional.ofNullable(taskRepository.findByIdUserId(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            task.setName(name);
            task.setDescription(description);
            update(task, entityManager);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph updateByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskGraphRepository(entityManager);
            @NotNull final TaskGraph task = Optional.ofNullable(taskRepository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            task.setName(name);
            task.setDescription(description);
            update(task, entityManager);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph startById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskGraphRepository(entityManager);
            @NotNull final TaskGraph task = Optional.ofNullable(taskRepository.findByIdUserId(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            update(task, entityManager);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph startByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskGraphRepository(entityManager);
            @NotNull final TaskGraph task = Optional.ofNullable(taskRepository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            update(task, entityManager);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph startByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskGraphRepository(entityManager);
            @NotNull final TaskGraph task = Optional.ofNullable(taskRepository.findByName(userId, name))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            update(task, entityManager);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph finishById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskGraphRepository(entityManager);
            @NotNull final TaskGraph task = Optional.ofNullable(taskRepository.findByIdUserId(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.COMPLETED);
            task.setFinishDate(new Date());
            update(task, entityManager);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph finishByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskGraphRepository(entityManager);
            @NotNull final TaskGraph task = Optional.ofNullable(taskRepository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.COMPLETED);
            task.setFinishDate(new Date());
            update(task, entityManager);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph finishByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskGraphRepository(entityManager);
            @NotNull final TaskGraph task = Optional.ofNullable(taskRepository.findByName(userId, name))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.COMPLETED);
            task.setFinishDate(new Date());
            update(task, entityManager);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Nullable
    public TaskGraph add(@NotNull UserGraph user, @Nullable String name, @Nullable String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final TaskGraph task = new TaskGraph(name, description);
        add(user, task);
        return (task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskGraph> findAll(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository repository = new TaskGraphRepository(entityManager);
            return repository.findAllByUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@NotNull final UserGraph user, @Nullable final Collection<TaskGraph> collection) {
        if (collection == null || collection.isEmpty()) return;
        for (TaskGraph item : collection) {
            item.setUser(user);
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskGraph add(@NotNull final UserGraph user, @Nullable final TaskGraph entity) {
        if (entity == null) return null;
        entity.setUser(user);
        @Nullable final TaskGraph entityResult = add(entity);
        return entityResult;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskGraph findById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository repository = new TaskGraphRepository(entityManager);
            return repository.findByIdUserId(userId, optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository repository = new TaskGraphRepository(entityManager);
            repository.clearByUserId(userId);
            @NotNull final List<TaskGraph> tasks = repository.findAllByUserId(userId);
            for (TaskGraph t :
                    tasks) {
                repository.remove(t);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository repository = new TaskGraphRepository(entityManager);
            @Nullable final TaskGraph task = repository.findByIdUserId(
                    userId,
                    optionalId.orElseThrow(EmptyIdException::new)
            );
            if (task == null) return;
            repository.remove(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final String userId, @Nullable final TaskGraph entity) {
        if (entity == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository repository = new TaskGraphRepository(entityManager);
            @Nullable final TaskGraph task = repository.findByIdUserId(userId, entity.getId());
            if (task == null) return;
            repository.remove(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
