package ru.konovalov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.IPropertyService;
import ru.konovalov.tm.api.repository.dto.ISessionRecordRepository;
import ru.konovalov.tm.api.service.IConnectionService;
import ru.konovalov.tm.api.service.dto.ISessionRecordService;
import ru.konovalov.tm.api.service.dto.IUserRecordService;
import ru.konovalov.tm.dto.SessionRecord;
import ru.konovalov.tm.dto.UserRecord;
import ru.konovalov.tm.enumerated.Role;
import ru.konovalov.tm.exception.empty.EmptyIdException;
import ru.konovalov.tm.exception.system.AccessDeniedException;
import ru.konovalov.tm.repository.dto.SessionRecordRepository;
import ru.konovalov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public final class SessionRecordService extends AbstractRecordService<SessionRecord> implements ISessionRecordService {

    @NotNull
    private final IUserRecordService userService;

    @NotNull
    private final IPropertyService propertyService;

    public SessionRecordService(
            @NotNull final IConnectionService connectionService,
            @NotNull IUserRecordService userService,
            @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionRecord> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<SessionRecord> collection) {
        if (collection == null || collection.isEmpty()) return;
        for (SessionRecord item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionRecord add(@Nullable final SessionRecord entity) {
        if (entity == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(entityManager);
            repository.add(entity);
            entityManager.getTransaction().commit();
            return entity;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionRecord findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(entityManager);
            return repository.findById(optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(entityManager);
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(entityManager);
            repository.removeById(optionalId.orElseThrow(EmptyIdException::new));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final SessionRecord entity) {
        if (entity == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(entityManager);
            repository.removeById(entity.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    @NotNull
    public SessionRecord open(@Nullable final String login, @Nullable final String password) {
        @Nullable final UserRecord user = checkDataAccess(login, password);
        if (user == null) throw new AccessDeniedException();
        final SessionRecord session = new SessionRecord();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        @Nullable final SessionRecord resultSession = sign(session);
        add(resultSession);
        return resultSession;
    }

    @Override
    @SneakyThrows
    public UserRecord checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        @Nullable final UserRecord user = userService.findByLogin(login);
        if (user == null) return null;
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null || hash.isEmpty() || user.isLocked()) return null;
        if (hash.equals(user.getPasswordHash())) {
            return user;
        } else {
            return null;
        }
    }

    @Override
    @SneakyThrows
    public void validate(@NotNull final SessionRecord session, final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final UserRecord user = userService.findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final SessionRecord session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final SessionRecord temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(entityManager);
            if (repository.findById(session.getId()) == null) throw new AccessDeniedException();
        } finally {
            entityManager.close();
        }

    }

    @Override
    @SneakyThrows
    @Nullable
    public SessionRecord sign(@Nullable final SessionRecord session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = HashUtil.sign(propertyService, session);
        session.setSignature(signature);
        return session;
    }

    @Override
    @SneakyThrows
    public void close(@Nullable final SessionRecord session) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(entityManager);
            repository.removeById(session.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void closeAllByUserId(@Nullable final String userId) {
        if (userId == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(entityManager);
            repository.removeByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    @Nullable
    public List<SessionRecord> findAllByUserId(@Nullable final String userId) {
        if (userId == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(entityManager);
            return repository.findAllByUserId(userId);
        } finally {
            entityManager.close();
        }
    }
}
