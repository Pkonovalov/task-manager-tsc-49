package ru.konovalov.tm.api.entity;

public interface IWBS extends IHasStartDate, IHasName, IHasCreated, IHasStatus {
}