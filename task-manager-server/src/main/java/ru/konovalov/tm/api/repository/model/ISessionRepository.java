package ru.konovalov.tm.api.repository.model;

import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.IRepository;
import ru.konovalov.tm.model.SessionGraph;

import java.util.List;

public interface ISessionRepository extends IRepository<SessionGraph> {

    List<SessionGraph> findAllByUserId(@Nullable String userId);

    void removeByUserId(@Nullable String userId);

    void update(final SessionGraph session);

}
