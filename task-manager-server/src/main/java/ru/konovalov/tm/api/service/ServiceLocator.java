package ru.konovalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.api.IPropertyService;
import ru.konovalov.tm.api.service.dto.*;

public interface ServiceLocator {

    @NotNull
    ITaskRecordService getTaskRecordService();

    @NotNull
    IProjectRecordService getProjectRecordService();

    @NotNull
    IProjectTaskRecordService getProjectTaskRecordService();

    @NotNull
    IUserRecordService getUserRecordService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISessionRecordService getSessionRecordService();

    @NotNull
    IDataService getDataService();

    @NotNull
    IConnectionService getConnectionService();
}
