package ru.konovalov.tm.api.repository.model;

import ru.konovalov.tm.api.IRepository;
import ru.konovalov.tm.model.UserGraph;

public interface IUserRepository extends IRepository<UserGraph> {

    UserGraph findByLogin(final String login);

    UserGraph findByEmail(final String email);

    void removeUserByLogin(final String login);

    void update(final UserGraph user);

}
