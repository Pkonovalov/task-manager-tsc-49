package ru.konovalov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.dto.LoggerDTO;

public interface IMessageService {

    void sendMessage(@NotNull LoggerDTO entity);

    @SneakyThrows
    LoggerDTO prepareMessage(@Nullable Object record,
                             @NotNull String type);
}
