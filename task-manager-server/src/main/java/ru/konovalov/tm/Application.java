package ru.konovalov.tm;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.bootstrap.Bootstrap;

public class Application {

    public static void main(final String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.initApplication();
        bootstrap.initJMSBroker();
        bootstrap.start(args);
    }

}