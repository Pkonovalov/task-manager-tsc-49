package ru.konovalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.IPropertyService;
import ru.konovalov.tm.endpoint.*;

public interface ServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    AdminEndpoint getAdminEndpoint();

    @NotNull
    DataEndpoint getDataEndpoint();

    @NotNull
    IPropertyService getPropertyService();

    @Nullable
    SessionDto getSession();

    void setSession(SessionDto session);
}
