package ru.konovalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.command.ProjectAbstractCommand;
import ru.konovalov.tm.endpoint.ProjectDto;
import ru.konovalov.tm.util.TerminalUtil;

public class ProjectCreateCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "project-create";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create project.";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final ProjectDto project = add(name, description);
        serviceLocator.getProjectEndpoint().addProject(getSession(), project);
    }
}
