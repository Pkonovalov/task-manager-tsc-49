package ru.konovalov.tm.command.user;

import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.command.AbstractCommand;
import ru.konovalov.tm.util.TerminalUtil;

public class UserLockByLoginCommand extends AbstractCommand {
    @Override
    public String name() {
        return "user-lock-by-login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Lock user by login";
    }

    @Override
    public void execute() {
        System.out.println("Enter login");
        @Nullable final String login = TerminalUtil.nextLine();
        serviceLocator.getAdminEndpoint().lockByLogin(getSession(), login);
    }

}
