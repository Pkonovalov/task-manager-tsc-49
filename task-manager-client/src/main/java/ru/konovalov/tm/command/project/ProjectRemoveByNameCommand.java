package ru.konovalov.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.command.ProjectAbstractCommand;
import ru.konovalov.tm.endpoint.ProjectDto;
import ru.konovalov.tm.exception.entity.ProjectNotFoundException;
import ru.konovalov.tm.util.TerminalUtil;

public class ProjectRemoveByNameCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove project by name.";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final ProjectDto project = serviceLocator.getProjectEndpoint().findProjectByName(getSession(), name);
        if (project == null) throw new ProjectNotFoundException();
        serviceLocator.getProjectEndpoint().removeProjectByName(getSession(), name);
    }
}
