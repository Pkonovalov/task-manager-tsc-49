package ru.konovalov.tm.command.auth;

import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.command.AuthAbstractCommand;
import ru.konovalov.tm.util.TerminalUtil;

public class ProfileUpdateCommand extends AuthAbstractCommand {
    @Override
    public String name() {
        return "update-profile";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update profile.";
    }

    @Override
    public void execute() {
        System.out.println("Enter first name");
        @Nullable final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter last name");
        @Nullable final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter middle name");
        @Nullable final String middleName = TerminalUtil.nextLine();
        serviceLocator.getSessionEndpoint().updateUser(getSession(), firstName, lastName, middleName);
    }
}
