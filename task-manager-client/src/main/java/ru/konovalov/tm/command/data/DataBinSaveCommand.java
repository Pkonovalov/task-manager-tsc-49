package ru.konovalov.tm.command.data;

import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.command.AuthAbstractCommand;

public class DataBinSaveCommand extends AuthAbstractCommand {

    @Nullable

    public String name() {
        return "data-save-bin";
    }

    @Nullable

    public String arg() {
        return null;
    }

    @Nullable

    public String description() {
        return "Save binary data";
    }

    public void execute() {
        serviceLocator.getDataEndpoint().saveDataBin(getSession());
    }

}