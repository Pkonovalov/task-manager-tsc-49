package ru.konovalov.tm.command.auth;

import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.command.AuthAbstractCommand;
import ru.konovalov.tm.endpoint.SessionDto;
import ru.konovalov.tm.util.TerminalUtil;

public class LoginCommand extends AuthAbstractCommand {

    @Override
    public String name() {
        return "login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Logging in to application.";
    }

    @Override
    public void execute() {
        System.out.println("Enter login");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("Enter password");
        @Nullable final String password = TerminalUtil.nextLine();
        SessionDto session = serviceLocator.getSessionEndpoint().open(login, password);
        serviceLocator.setSession(session);
    }

}
