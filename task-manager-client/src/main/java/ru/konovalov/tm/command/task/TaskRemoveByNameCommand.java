package ru.konovalov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.command.TaskAbstractCommand;
import ru.konovalov.tm.util.TerminalUtil;

public class TaskRemoveByNameCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove task by name.";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        serviceLocator.getTaskEndpoint().removeTaskByName(getSession(), name);
    }
}
