package ru.konovalov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.command.TaskAbstractCommand;
import ru.konovalov.tm.endpoint.TaskDto;
import ru.konovalov.tm.exception.entity.TaskNotFoundException;
import ru.konovalov.tm.util.TerminalUtil;

import static ru.konovalov.tm.util.TerminalUtil.incorrectValue;

public class TaskUpdateByIndexCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-update-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update task by index.";
    }

    @Override
    public void execute() {
        System.out.println("Enter index");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final TaskDto task = serviceLocator.getTaskEndpoint().findTaskByIndex(getSession(), index);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final TaskDto taskUpdated = serviceLocator.getTaskEndpoint().updateTaskByIndex(getSession(), index, name, description);
        if (taskUpdated == null) incorrectValue();
    }
}
