package ru.konovalov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    @NotNull
    public IndexIncorrectException() {
        super("Error. Index is incorrect.");
    }

    @NotNull
    public IndexIncorrectException(@NotNull final Throwable cause) {
        super(cause);
    }

    @NotNull
    public IndexIncorrectException(@NotNull final String value) {
        super("Error. This value `" + value + "` is not number.");
    }

}
