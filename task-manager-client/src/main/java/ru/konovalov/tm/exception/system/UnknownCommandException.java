package ru.konovalov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.constant.TerminalConst;
import ru.konovalov.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    @NotNull
    public UnknownCommandException(@NotNull String command) {
        super("Incorrect command '" + command + "'. Use " + TerminalConst.CMD_HELP + " for display list of terminal commands.");
    }

}
