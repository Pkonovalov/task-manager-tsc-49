package ru.konovalov.tm.constant;

public interface TerminalConst {

    String CMD_HELP = "help";

    String CMD_EXIT = "exit";

}
