package ru.konovalov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.bootstrap.Bootstrap;
import ru.konovalov.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class FileScanner {

    @NotNull
    private static final String PATH = "./";

    private static final int INTERVAL = 10;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final Bootstrap bootstrap;

    public FileScanner(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        commands.addAll(
                bootstrap.getCommandService().getArguments().stream()
                        .map(AbstractCommand::name)
                        .collect(Collectors.toList())
        );
        es.scheduleWithFixedDelay(this::run, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void run() {
        @NotNull final File file = new File(PATH);
        Arrays.stream(file.listFiles())
                .filter(o -> o.isFile() && commands.contains(o.getName()))
                .forEach(o -> {
                    bootstrap.runCommand(o.getName());
                    o.delete();
                });
    }

}