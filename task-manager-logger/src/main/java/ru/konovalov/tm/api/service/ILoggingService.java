package ru.konovalov.tm.api.service;
import ru.konovalov.tm.dto.LoggerDTO;

public interface ILoggingService {

    void writeLog(LoggerDTO message);

}
