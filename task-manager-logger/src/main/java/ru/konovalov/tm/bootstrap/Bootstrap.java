package ru.konovalov.tm.bootstrap;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.api.service.IReceiverService;
import ru.konovalov.tm.listener.LogMessageListener;
import ru.konovalov.tm.service.ReceiverService;

import static ru.konovalov.tm.constant.ActiveMQConst.URL;

public final class Bootstrap {

    public void start() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(URL);
        factory.setTrustAllPackages(true);
        @NotNull final IReceiverService receiverService = new ReceiverService(factory);
        ((ReceiverService) receiverService).receive(new LogMessageListener());
    }

}
