package ru.konovalov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.api.service.IReceiverService;

import javax.jms.*;

import static com.sun.org.apache.xalan.internal.xsltc.compiler.Constants.STRING;

public class ReceiverService implements IReceiverService {

    @NotNull
    private final ConnectionFactory connectionFactory;

    public ReceiverService(@NotNull ConnectionFactory factory) {
        connectionFactory = factory;
    }

    @SneakyThrows
    public void receive(@NotNull final MessageListener listener) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();

        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic(STRING);
        final MessageConsumer consumer = session.createConsumer(destination);
        consumer.setMessageListener(listener);
    }

}
